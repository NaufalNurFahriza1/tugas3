import React, { useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import LinearGradient from 'react-native-linear-gradient';

const ClipBoards = () => {
    const [copiedText, setCopiedText] = useState('');

    const copyToClipboard = () => {
        Clipboard.setString('Hello world');
    };

    const fetchCopiedText = async () => {
        const text = await Clipboard.getString();
        setCopiedText(text);
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.box}>
                <Text style={{fontWeight:'bold'}}>8. Menerapkan ClipBoard</Text>
                <TouchableOpacity onPress={copyToClipboard}>
                    <Text>Click here to copy to Clipboard</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={fetchCopiedText}>
                    <Text>View copied text</Text>
                </TouchableOpacity>
                <Text style={styles.copiedText}>{copiedText}</Text>
            </View>

            <View style={styles.box}>
                <Text style={{fontWeight:'bold'}}>9. Menerapkan Linear Gradient</Text>
            <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>
                <Text style={styles.buttonText}>
                    Sign in with Facebook
                </Text>
            </LinearGradient>
            </View>


        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'center',
    },

    box: {
        padding: 5,
        margin: 10,
        display: 'flex',
        borderWidth: 2,
        borderRadius: 5,
        borderwidth: '90%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    copiedText: {
        marginTop: 10,
        color: 'red',
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
});

export default ClipBoards;