import React, { useState } from 'react';
import { StyleSheet, View, Text, Button, Platform, } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { ProgressBar } from '@react-native-community/progress-bar-android';

const DatePicker = (navigation) => {
  const [isPickerShow, setIsPickerShow] = useState(false);
  const [date, setDate] = useState(new Date(Date.now()));

  const showPicker = () => {
    setIsPickerShow(true);
  };

  const onChange = (event, value) => {
    setDate(value);
    if (Platform.OS === 'android') {
      setIsPickerShow(false);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <Text style={{fontWeight:'bold'}}>2. Menerapkan Date Picker</Text>
        {/* Display the selected date */}
        <View style={styles.pickedDateContainer}>
          <Text style={styles.pickedDate}>{date.toUTCString()}</Text>
        </View>

        {/* The button that used to trigger the date picker */}
        {!isPickerShow && (
          <View style={styles.btnContainer}>
            <Button title="Show Picker" color="purple" onPress={showPicker} />
          </View>
        )}

        {/* The date picker */}
        {isPickerShow && (
          <DateTimePicker
            value={date}
            mode={'date'}
            display={Platform.OS === 'ios' ? 'spinner' : 'default'}
            is24Hour={true}
            onChange={onChange}
            style={styles.datePicker}
          />
        )}
      </View>
      <View style={styles.box}>
      <Text style={{fontWeight:'bold'}}>3. Menerapkan Progress Bar Android</Text>
        <View style={styles.example}>
          <Text>Circle Progress Indicator</Text>
          <ProgressBar />
        </View>
        <View style={styles.example}>
          <Text>Horizontal Progress Indicator</Text>
          <ProgressBar styleAttr="Horizontal" />
        </View>
        <View style={styles.example}>
          <Text>Colored Progress Indicator</Text>
          <ProgressBar styleAttr="Horizontal" color="#2196F3" />
        </View>
        <View style={styles.example}>
          <Text>Fixed Progress Value</Text>
          <ProgressBar
            styleAttr="Horizontal"
            indeterminate={false}
            progress={0.5}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    justifyContent: 'center',
  },

  box: {
    padding: 5,
    margin: 10,
    display: 'flex',
    borderWidth: 2,
    borderRadius: 5,
    borderwidth: '90%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  pickedDateContainer: {
    padding: 20,
    backgroundColor: '#eee',
    borderRadius: 10,
  },
  pickedDate: {
    fontSize: 18,
    color: 'black',
  },
  btnContainer: {
    padding: 30,
  },
  example: {
    marginVertical: 24,
  },
  // This only works on iOS
  datePicker: {
    width: 320,
    height: 260,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});

export default DatePicker;