import React, { useState } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import { ProgressView } from "@react-native-community/progress-view";


export default function GeoLoc() {
    const [location, setLocation] = useState(null);

    function handlePress() {
        Geolocation.getCurrentPosition(
            position => {
                setLocation(position.coords);
            },
            error => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }

    return (
        <View style={styles.container}>
            <View style={styles.box}>
                <Text style={{fontWeight:'bold'}}>6. Menerapkan Geolocation</Text>
                <Button title="Get Location" onPress={handlePress} />
                {location && (
                    <View>
                        <Text>Latitude: {location.latitude}</Text>
                        <Text>Longitude: {location.longitude}</Text>
                    </View>
                )}
            </View>
            <Text style={{fontWeight:'bold', alignItems:'center', margin:20}}>7. Menerapkan Progress View</Text>
                <ProgressView
                    progressTintColor="orange"
                    trackTintColor="blue"
                    progress={0.7}
                />
            </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'center',
    },

    box: {
        padding: 5,
        margin: 10,
        display: 'flex',
        borderWidth: 2,
        borderRadius: 5,
        borderwidth: '90%',
        alignItems: 'center',
        justifyContent: 'center',
    },

});