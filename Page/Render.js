import React from 'react';
import {useWindowDimensions} from 'react-native';
import RenderHtml from 'react-native-render-html';

const source = {
  html: `
  <p style='text-align:center; fontWeight:bold'>
    10. Menerapkan Render HTML!
  </p>`,
};


export default function Render() {
  const {width} = useWindowDimensions();
  return (
      <RenderHtml contentWidth={width} source={source} />
  );
}
